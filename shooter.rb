require 'rubygems'
require 'gosu'

require_relative 'collide'

class Shooter

  include Collide
  
  attr_reader :x, :y, :speed, :delay
  attr_accessor :image

  def initialize(x, y)
    @x = x
    @y = y
    @speed = 5
    @delay = 10
  end



  def height
    @image.height
  end

  def width
    @image.width
  end

  def move_left(boundry)
    @x = @x - @speed
    @x = boundry if @x < boundry
  end

  def move_right(boundry)
    @x = @x + @speed
    @x = boundry if @x > boundry
  end

  def can_shoot?
    true if @delay <= 0
  end

  def update
    @delay = @delay - 1 unless @delay <= 0
  end

  def add_delay(amount)
    @delay = @delay + amount
  end

end
  
  
    
