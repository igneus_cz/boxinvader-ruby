
#Assumed that objects have these variables
# x, y, height, and width
# Where x and y are the top left corner location

module Collide
 
      
  def collided_with?(other)

    if x_within?(other) and y_within?(other)
      true
    else
      false
    end

 end

 #Find which has the smallest height, aka y range
 #Then checks to see if that y domain is inside the larger object
 def y_within?(other)

   if (height >= other.height)
     within?(other.y, other.y + other.height, y, y + height)
   else
     within?(y, y + height, other.y, other.y + other.height)
   end
 end

 #Find which has the smallest width, aka x range
 #Then checks to see if that domain is within the domain of the larger object
 def x_within?(other)

   if (width >= other.width)
     within?(other.x, other.x + other.width, x, x + width)
   else
     within?(x, x + width, other.x, other.x + other.width)
   end
 end


 #Then function that actually does the checking of domain within
 #First two parameters are the smaller object domain
 #It's then compared to the larger object to see if they are within that domain
 def within?(x1, x2, x3, x4)
    (x1 >= x3 && x1 <= x4) || (x2 >=x3 && x2 <= x4)
 end

end 
